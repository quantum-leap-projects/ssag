# Small size automated garden

A system to automate watering and supervision of garden plants for small areas like balconies or indoors.

The aim of this project is to water plants in a modular and automated fashion with a scope to small scale gardens, i.e. the setup can be powered with just a powerbank and a water tank over several days.  


## Setup

OnCopy the code to your Raspberry PI and run in the main/src/pump folder the command

    pip3 install -r requirements.txt

This service runs as systemd service, run 

    sudo nano /etc/systemd/system/plant.service

and copy 

    [Unit]
    Description=Run pump to water plants
    DefaultDependencies=no
    After=network.target

    [Service]
    Type=simple
    ExecStart=sudo python3 /home/pi/tmbb/src/main/EnvironmentInfoMain.py
    TimeoutStartSec=0
    RemainAfterExit=yes
    WorkingDirectory=/home/pi

    [Install]
    WantedBy=default.target