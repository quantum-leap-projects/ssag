import time


class Timer:
    def __init__(self, interval):
        self.interval = interval
        self.start_time = time.time()

    def time_left(self):
        elapsed_time = time.time() - self.start_time
        time_left = max(0, self.interval - elapsed_time)
        return time_left

    def reset(self):
        self.interval = 0
