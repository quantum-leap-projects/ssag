import asyncio
import math
import socket
from typing import Annotated

from fastapi import FastAPI, APIRouter, Form
from starlette.responses import HTMLResponse

from ConfigHandler import ConfigHandler, PumpConfig
from PumpControl import PumpControl
from Timer import Timer
from gpioInterface import GpioHandlerFactory
from gpioInterface.GpioHandlerType import GpioHandlerType


class Pump(FastAPI):
    timer: Timer = Timer(0)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.current_task = None
        api_router = APIRouter()
        api_router.add_api_route("/", self.root, methods=["GET"])
        api_router.add_api_route("/run", self.run, methods=["POST"])
        api_router.add_api_route("/write_config", self.write_config, methods=["POST"])
        api_router.add_api_route("/state", self.get_state, methods=["GET"])
        api_router.add_api_route("/current_job", self.get_current_job, methods=["GET"])
        api_router.add_api_route("/run_day", self.run_day, methods=["GET"])
        self.include_router(api_router)

        self.gpio = GpioHandlerFactory.create_gpio_handler(GpioHandlerType.DUMMY)
        self.config_loader = ConfigHandler()
        self.pump_config = self.config_loader.load()
        self.pump = PumpControl(configuration=self.pump_config, gpio=self.gpio)

        @self.on_event("startup")
        async def run_pump_scheduler():
            await self.run_day()

    async def root(self):
        return await self.create_main_page()

    async def create_main_page(self):
        hour = self.pump_config.hour
        minute = self.pump_config.minute
        interval = self.pump_config.interval
        pump_name = self.pump_config.name
        ip = self.get_ip_address()
        main_page = f"""
        <html>
            <head>
                <title>Pump control for water supply</title>
            </head>
            <body>
                <h1>Pump control for {pump_name} water supply</h1>
                <p>See <a href="http://{ip}/docs">http://{ip}/docs</a> for API overview</p>
                
                <!-- Button to trigger the Run API -->
                <form action="/run" method="post">
                    <label for="interval">Interval:</label>
                    <input type="number" id="interval" name="interval" value="60" required>
                    <button type="submit">Run</button>
                </form>

                <!-- Button to trigger the Write Config API -->
                <form action="/write_config" method="post">
                    <label for="hour">Hour:</label>
                    <input type="number" id="hour" name="hour" value="{hour}">
                    <label for="minute">Minute:</label>
                    <input type="number" id="minute" name="minute" value="{minute}">
                    <label for="interval">Interval:</label>
                    <input type="number" id="interval" name="interval" value="{interval}">
                    <label for="pump_name">Pump Name:</label>
                    <input type="text" id="pump_name" name="pump_name" value="{pump_name}">
                    <button type="submit">Write Config</button>
                </form>

                <!-- Button to trigger the Get State API -->
                <form action="/state" method="get">
                    <button type="submit">Get State</button>
                </form>

                <!-- Button to trigger the Get Current Job API -->
                <form action="/current_job" method="get">
                    <button type="submit">Get Current Job</button>
                </form>

            </body>
        </html>
        """
        return HTMLResponse(content=main_page, status_code=200)

    async def run(self, interval: Annotated[int, Form()]):
        self.current_task = asyncio.create_task(self.async_pump(interval))
        self.timer = Timer(interval)
        return {"pump is running": True}

    async def run_day(self):
        if self.current_task is None:
            self.current_task = asyncio.create_task(self.create_schedule())
            return {"pump is running": True}
        return {"pump is already running": True}

    async def count_down(self):
        if self.timer is not None:
            return self.timer.time_left()
        else:
            return 0

    async def async_pump(self, interval):
        await asyncio.to_thread(self.pump.run_pump, interval)

    async def create_schedule(self):
        await asyncio.to_thread(self.pump.schedule)

    async def get_state(self):
        return {
            "Pump Config": self.pump_config,
            "Job": await self.is_pumping(),
            "Job finished in": math.ceil(await self.count_down())}

    async def get_current_job(self):
        return {
            "Job": await self.is_pumping(),
            "Job finished in": math.ceil(await self.count_down())}

    async def is_pumping(self):
        return self.pump.is_running

    async def write_config(self, hour: Annotated[int, Form()], minute: Annotated[int, Form()],
                           interval: Annotated[int, Form()], pump_name: Annotated[str, Form()]):
        pump_config = PumpConfig()
        pump_config.set_hour(hour)
        pump_config.set_minute(minute)
        pump_config.set_interval(interval)
        pump_config.name = pump_name
        pump_config.create_daytime()
        self.pump_config = pump_config
        self.config_loader.write_config(pump_config)
        main_page = await self.create_main_page()
        return main_page

    def get_ip_address(self):
        hostname = socket.gethostname()
        ip_address = socket.gethostbyname(hostname)
        return ip_address
