import logging

import yaml


class PumpConfig:
    hour: int
    minute: int
    interval: int
    daytime: str
    name: str

    def set_hour(self, hour):
        self.hour = hour

    def set_minute(self, minute):
        self.minute = minute

    def set_interval(self, interval):
        self.interval = interval

    def create_daytime(self):
        self.daytime = '{}:{}'.format(self.hour, self.minute)

    def to_dict(self):
        # Convert the class attributes to a nested dictionary format matching the required YAML structure
        return {
            'name': self.name,
            'time': {
                'daytime': {
                    'hour': self.hour,
                    'minute': self.minute
                },
                'interval': self.interval
            }
        }


class ConfigHandler:

    def __init__(self):
        self.config_file = None

    def load(self, configuration="config.yml") -> PumpConfig:
        logging.info("Loading config from ", configuration)

        with open(configuration, 'r') as configuration:
            self.config_file = yaml.safe_load(configuration)

        config = self._create_pump_config()
        self._set_daytime(config)
        self._set_interval(config)
        config.create_daytime()
        config.name = self.config_file["name"]
        return config

    def _set_interval(self, config):
        config.set_interval(self.config_file['time']['interval'])

    def _set_daytime(self, config):
        config.set_minute(self.config_file['time']['daytime']['minute'])
        config.set_hour(self.config_file['time']['daytime']['hour'])
        config.create_daytime()

    def _create_pump_config(self):
        config = PumpConfig()
        return config

    def write_config(self, pump_config, filename="config.yml"):
        with open(filename, 'w') as file:
            yaml.dump(pump_config.to_dict(), file)
