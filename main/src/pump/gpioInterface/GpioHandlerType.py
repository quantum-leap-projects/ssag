from enum import Enum, auto


class GpioHandlerType(Enum):
    DUMMY = auto()
    DEFAULT = auto()
