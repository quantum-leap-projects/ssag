from typing import Protocol


class GpioHandler(Protocol):
    def disable_power(self) -> None:
        ...

    def enable_power(self) -> None:
        ...
