# dummy for unit tests and test purposes
class DummyGpioHandler:
    def __init__(self):
        pass

    def disable_power(self) -> None:
        print("Power turned off.")

    def enable_power(self) -> None:
        print("Power turned on.")
