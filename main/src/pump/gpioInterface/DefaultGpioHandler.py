from RPi import GPIO


class DefaultGpioHandler:

    def __init__(self):
        self.RELAIS_1_GPIO = 17

        GPIO.setmode(GPIO.BCM)  # GPIO Numbers instead of board numbers
        GPIO.setup(self.RELAIS_1_GPIO, GPIO.OUT)  # GPIO Assign mode
        GPIO.output(self.RELAIS_1_GPIO, GPIO.LOW)  # in case of failure make of

    def disable_power(self):
        GPIO.output(self.RELAIS_1_GPIO, GPIO.LOW)

    def enable_power(self):
        GPIO.output(self.RELAIS_1_GPIO, GPIO.HIGH)
