from gpioInterface.GpioHandler import GpioHandler
from gpioInterface.GpioHandlerType import GpioHandlerType


def create_gpio_handler(gpio_handler_type: GpioHandlerType) -> GpioHandler:
    handlers = {
        GpioHandlerType.DUMMY: __dummy_handler(),
        GpioHandlerType.DEFAULT: __default_handler()
    }
    return handlers.get(gpio_handler_type)()


def __dummy_handler():
    return lambda: __create_dummy_handler()


def __default_handler():
    return lambda: __create_default_handler()


def __create_dummy_handler():
    from gpioInterface.DummyGpioHandler import DummyGpioHandler
    return DummyGpioHandler()


def __create_default_handler():
    from gpioInterface.DefaultGpioHandler import DefaultGpioHandler
    return DefaultGpioHandler()
