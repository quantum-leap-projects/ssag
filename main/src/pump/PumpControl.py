import logging
import time

import schedule

from ConfigHandler import PumpConfig
from gpioInterface import GpioHandler

logging.basicConfig(filename="../control.log")


class PumpControl:
    is_running = False
    pump_config: PumpConfig = None

    def __init__(self, configuration: PumpConfig, gpio: GpioHandler):
        self.pump_config = configuration
        self._gpio = gpio
        # in case of program reboot, reset GPIO pin.
        self.__disable_power()
        logging.info("Schedule pump configuration.")
        self.thread = schedule.Scheduler()
        self.thread.every().day.at(self.get_daytime).do(self.run_pump, self.get_interval).tag('daily-tasks')

    @property
    def get_daytime(self):
        return self.pump_config.daytime

    @property
    def get_interval(self):
        return self.pump_config.interval

    def run_pump(self, interval: int):
        if self.is_running is False:
            self.is_running = True
            self.__enable_power()
            time.sleep(interval)
            self.__disable_power()
            self.is_running = False

    def schedule(self):
        while True:
            self.thread.run_pending()
            logging.debug("schedule check")
            time.sleep(3)

    def __enable_power(self):
        self._gpio.enable_power()

    def __disable_power(self):
        self._gpio.disable_power()

    async def has_job(self):
        return self.is_running
