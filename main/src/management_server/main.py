import uvicorn
from app import AppServer

if __name__ == '__main__':
    try:
        server = uvicorn.Server
        app = AppServer()
        uvicorn.run(app, host='0.0.0.0', port=8000, access_log=True, log_config="./pump/log_config.yml")
    except KeyboardInterrupt:
        uvicorn.Server.handle_exit()