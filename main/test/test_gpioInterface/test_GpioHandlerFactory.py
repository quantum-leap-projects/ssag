from unittest import TestCase

from gpioInterface import GpioHandlerFactory
from gpioInterface.DummyGpioHandler import DummyGpioHandler
from gpioInterface.GpioHandlerType import GpioHandlerType


class TestGpioHandlerFactory(TestCase):
    def test_create_gpio_handler__dummy_type_requested__dummyGpioHandler_returned(self):
        actual = GpioHandlerFactory.create_gpio_handler(GpioHandlerType.DUMMY)

        self.assertTrue(isinstance(actual, DummyGpioHandler))
