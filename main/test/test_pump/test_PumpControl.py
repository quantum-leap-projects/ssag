from unittest import TestCase

from ConfigLoader import PumpConfig, ConfigLoader
from PumpControl import PumpControl
from gpioInterface import GpioHandlerFactory
from gpioInterface.GpioHandlerType import GpioHandlerType


class TestPumpControl(TestCase):
    async def test_run_pump__called__hasJob(self):
        gpio_handler = GpioHandlerFactory.create_gpio_handler(GpioHandlerType.DUMMY)
        pump_config: PumpConfig = ConfigLoader().load("./test_pump/default_config.yml")
        seconds = 1
        underTest = PumpControl(pump_config, gpio_handler)
        await underTest.run_pump(seconds)

        actual = underTest.has_job()

        self.assertTrue(actual)
