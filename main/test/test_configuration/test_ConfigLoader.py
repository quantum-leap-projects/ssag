from unittest import TestCase

from main.src.pump import ConfigHandler


class TestConfigHandler(TestCase):
    def test_load__late_daytime_time_set__config_contains_time_set(self):
        file = "./test_configuration/config_late.yml"
        expected = ConfigHandler.PumpConfig()
        expected.daytime = '{}:{}'.format(10, 32)

        underTest = ConfigHandler.ConfigHandler()

        actual: ConfigHandler.PumpConfig = underTest.load(file)

        self.assertEqual(expected.daytime, actual.daytime)

    def test_load__early_daytime_time_set__config_contains_time_set(self):
        file = "./test_configuration/config_early.yml"
        expected = ConfigHandler.PumpConfig()
        expected.daytime = '{}:{}'.format(8, 5)

        underTest = ConfigHandler.ConfigHandler()

        actual: ConfigHandler.PumpConfig = underTest.load(file)

        self.assertEqual(expected.daytime, actual.daytime)